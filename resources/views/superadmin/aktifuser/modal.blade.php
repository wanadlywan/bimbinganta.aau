<form action="{{url('proc-aktif-user')}}" id="formAktifUser" method="POST" class="form-horizontal">
    @csrf
    <div class="form-body">
        <div class="form-group">
            <label class="col-md-3 control-label">Nama Mahasiswa</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" readonly class="form-control input-circle-left" placeholder="Nama" name="name" value="{{$mahasiswa->name}}">
                    <input type="hidden" name="id_mhs" value="{{$mahasiswa->id}}">
                    <input type="hidden" name="status" value="{{$mahasiswa->status}}">
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-user"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">DosPem 1</label>
            <div class="col-md-9">
                <div class="input-group">
                    <select class="form-control input-circle-left" name="id_dosen1">
                        <option>-- Pilih Dosen --</option>
                        @foreach($dosen as $key => $val):
                            <option value="{{$val->id}}" @if(isset($dsnMhs->id_dosen1) && $dsnMhs->id_dosen1 == $val->id) selected @endif >{{$val->name}}</option>    
                        @endforeach
                    </select>
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-user"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">DosPem 2</label>
            <div class="col-md-9">
                <div class="input-group">
                    <select class="form-control input-circle-left" name="id_dosen2">
                        <option>-- Pilih Dosen --</option>
                        @foreach($dosen as $key => $val):
                            <option value="{{$val->id}}" @if(isset($dsnMhs->id_dosen2) && $dsnMhs->id_dosen2 == $val->id) selected @endif >{{$val->name}}</option>    
                        @endforeach
                    </select>
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-user"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    @if($mahasiswa->status == 0)
    <div class="modal-footer">
        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Keluar</button>
        <button type="button" onClick="procAktifUser()"  class="btn green">Aktif</button>
    </div>
    @endif
</form>