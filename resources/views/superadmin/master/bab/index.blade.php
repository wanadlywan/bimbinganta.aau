@extends('layouts.header')

@section('content')
<div class="page-content">
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{$title}}</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                <i class="icon-calendar"></i>&nbsp;
                <span class="thin uppercase hidden-xs"></span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </div>
        </div>
    </div>
    <h3 class="page-title"> {{$title}}</h3>
    
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Data BAB</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                        </div>
                    </div>
                </div>
                    <div id="notif_alert" class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> <p id="notif"> You have some form errors. Please check below.</p> 
                    </div>
                
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <!-- <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                        <i class="fa fa-plus"></i>
                                    </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="items">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th> BAB </th>
                                    <th> tgl awal </th>
                                    <th> tgl akhir </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bab as $key => $val):
                                  <tr>
                                      <td>{{$no++}}</td>
                                      <td>{{$val->bab}}</td>
                                      <td>{{$val->tgl_awal}}</td>
                                      <td>{{$val->tgl_akhir}}</td>
                                      <td>
                                          <button type="button" class="btn red btn-sm mt-ladda-btn ladda-button btn-circle" onClick="edit({{$val->id}})" data-style="zoom-in">
                                            <span class="ladda-label"><i class="fa fa-edit"></i> Edit</span>
                                          </button>
                                      </td>
                                  </tr>  
                                @endforeach

                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
   
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal Title</h4>
            </div>
            <div class="modal-body" id="content"> 
                <div id="loading">
                    <img src="/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                </div>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

function edit(e){
    var id = e
    var title = 'mst_bab'

    $('#basic').modal('show')
    $.ajax({
        type: 'GET',
        url : '{{url('getModalBab/')}}',
        data: {id:id,title:title},
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            $('#loading').hide()
            $('#content').html(data)
        }
    });    
}

function procEditBab(){
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url : $('#formAktifUser').attr('action'),
        data: $('#formAktifUser').serialize()+'&token='+token,
        dataType: 'json',
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            $('#basic').modal('hide')
            $('#loading').hide()
            $('#items').load(location.href + ' #items')
            $('#notif').html(data.notif)
            $('#notif_alert').addClass(data.alert)
        }
    });    
}


</script>

@endsection