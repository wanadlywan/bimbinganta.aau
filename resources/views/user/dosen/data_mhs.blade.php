
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                </div>
                    <div id="notif_alert" class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> <p id="notif"> You have some form errors. Please check below.</p> 
                    </div>
                
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <!-- <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                        <i class="fa fa-plus"></i>
                                    </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="items" class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th> Nama Mahasiswa </th>
                                    <th> Dospem 1 </th>
                                    <th> Dospem 2 </th>
                                    <th> Keterangan </th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($list_mhs as $val)
                                <tr class="odd gradeX">
                                    <td>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="checkboxes" value="1" />
                                            <span></span>
                                        </label>
                                    </td>
                                    <td> {{ $user_name[$val->id_mhs] }} </td>
                                    <td> {{ $user_name[$val->id_dosen1] }} </td>
                                    <td> {{ $user_name[$val->id_dosen2] }} </td>
                                    <td class="center"> {{ $dropStatus[$val->status] }} </td>
                                    <td>
                                         <button type="button" class="btn red btn-sm mt-ladda-btn ladda-button btn-circle"  onClick="DosenMhsProgTa({{$val->id_mhs}})"  data-style="zoom-in">
                                            <span class="ladda-label"><i class="fa fa-edit"></i> Detail</span>
                                          </button>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
   
</div>

<script>

function DosenMhsProgTa(e){
    id_mhs = e;
// alert(id_dosen_mhs);
    $('#basic').modal('show')
    $.ajax({
        type: 'GET',
        url : '{{url('dosen/proses-ta/')}}',
        data: {id_mhs:id_mhs},
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            $('#loading').hide()
            $('div').removeClass('modal-backdrop fade in')
            $('#subContentTa').html(data)
        }
    });    
}

function procTa(){
    var token = $('meta[name="csrf-token"]').attr('content');
    var komen = $('#komen').val();
    var id_progTa = $('#id_progTa').val();
    var id_proses = $('#id_proses').val();
    var fd = new FormData();
    var files = $('#upload')[0].files[0];
    fd.append('upload',files);
    fd.append('_token',token);
    fd.append('komen',komen);
    fd.append('id_progTa',id_progTa);
    fd.append('id_proses',id_proses);

    $.ajax({
        type: 'POST',
        url : $('#formProgTa').attr('action'),
        data: fd,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            alert(data.notif)
            $('div').removeClass('modal-backdrop fade in')
            $('#basic').modal('hide')
            $('#loading').hide()
            DosenMhsProgTa(id_mhs)
            $('#notif_alert').show()
            $('#notif').html(data.notif)
            $('#notif_alert').addClass(data.alert)
        }
    });    
}

</script>
