
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-arrow-left"></i>
                        <a class="caption-subject bold uppercase" onClick="back()"> {{$title}}</a>
                    </div>
                </div>
                    <div id="notif_alert" class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> <p id="notif"> You have some form errors. Please check below.</p> 
                    </div>
                
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="items" class="table-responsive">
                    <div class="col-md-6">
                        <form action="{{url('proc-bimbingan-ta')}}" id="formBimbinganTa" method="POST" enctype="multipart/form-data" class="form-horizontal">
                            @csrf
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kode</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" placeholder="kode" id="kode" name="kode" value="">
                                            <input type="hidden" name="id_proses" id="id_proses" value="{{$proses['id_proses']}}">
                                            <input type="hidden" name="id_progTa" id="id_progTa" value="{{$progTa->id}}">
                                            <span class="input-group-addon input-circle-right">
                                                <i class="fa fa-key"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Materi Bimbingan</label>
                                    <div class="col-md-9">
                                        <textarea id="bimbingan" name="bimbingan" class="form-control" id="bimbingan" cols="30" rows="10"></textarea>   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9">
                                        <button type="button" class="btn blue btn-sm mt-ladda-btn ladda-button btn-circle pull-right" onclick="procTaBimbingan()" data-style="zoom-in">
                                            <span class="ladda-label"><i class="fa fa-save"></i> Save</span>
                                        </button>
                                    </div>  
                                </div>
                                <hr>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bubble font-hide hide"></i>
                                    <span class="caption-subject font-hide bold uppercase">Keterangan</span>
                                </div>
                                <div class="actions">
                                    <div class="portlet-input input-inline">
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body" id="chats">
                                <div class="scroller" style="height: 550px;overflow: scroll;" data-always-visible="1" data-rail-visible1="1">
                                    <ul class="chats">
                                    @if(!empty($prosesBimbingan))
                                        @foreach($prosesBimbingan as $key => $val)
                                        @if(Auth::user()->id == $val['id_user'])
                                        <li class="in">
                                        @else
                                        <li class="out">
                                        @endif
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> {{$dropUser[$val['id_user']]}} </a>
                                                <span class="body"> {{$val['bimbingan']}} <i class="badge badge-danger">{{$val['kode']}}</i></span>
                                            </div>
                                        </li>
                                        @endforeach
                                    @endif           
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
   
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal Title</h4>
            </div>
            <div class="modal-body" id="content"> 
                <div id="loading">
                    <img src="/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                </div>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

function detailBimbingan(e,x){
    var id_proses = e;
    var id_progTa = x;
    $.ajax({
        type: 'GET',
        url : '{{url('getInputBimbingan/')}}',
        data: {id_proses:id_proses,id_progTa:id_progTa},
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            $('#loading').hide()
            $('#subContentBimTa').html(data)
        }
    });    
}

function procTaBimbingan(){
    var id_proses = $('#id_proses').val()
    var id_progTa = $('#id_progTa').val()
    var token = $('meta[name="csrf-token"]').attr('content');
    var kode = $('#kode').val()
    var bimbingan = $('#bimbingan').val()

    if(kode == '' || bimbingan == ''){
        alert('Harap Lengkapi data !')
        return false;
    }

    $.ajax({
        type: 'POST',
        url : $('#formBimbinganTa').attr('action'),
        data: $('#formBimbinganTa').serialize()+'&token='+token,
        dataType: 'json',
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            $('#loading').hide()
            $('#notif_alert').show()
            $('#notif').html(data.notif)
            $('#notif_alert').addClass(data.alert)
            detailBimbingan(id_proses,id_progTa)
        }
    });    
}

function deleteBimbingan(id){
    var id_proses = $('#id_proses').val()
    var id_progTa = $('#id_progTa').val()
    var token = $('meta[name="csrf-token"]').attr('content');

    if(confirm('Anda yakin akan menghapus data ini?')) {
        $.ajax({
            type: 'POST',
            url : '{{url("/delete-bimbingan")}}',
            data: $('#formBimbinganTa').serialize()+'&token='+token+'&id='+id,
            dataType: 'json',
            beforeSend:function(){
                $('#loading').show()
            },
            error: function(jqXHR, textStatus, errorThrown){
                $('#loading').hide()
                    
            },
            success: function(data){
                $('#loading').hide()
                $('#notif_alert').show()
                $('#notif').html(data.notif)
                $('#notif_alert').addClass(data.alert)
                detailBimbingan(id_proses,id_progTa)
            }
        }); 
    }
}


function back(){
    id = {{Auth::user()->id}}
    bimbingan_ta(id)
}

</script>
