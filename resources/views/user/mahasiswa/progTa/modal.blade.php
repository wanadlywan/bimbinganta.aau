<form action="{{url('proc-proggres-ta')}}" id="formProgTa" method="POST" enctype="multipart/form-data" class="form-horizontal">
    @csrf
    <div class="form-body">
        <div class="form-group">
            <label class="col-md-3 control-label">BAB</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control input-circle-left"readonly placeholder="BAB" name="bab" value="{{$proses['bab']}}">
                    <input type="hidden" name="id_proses" id="id_proses" value="{{$proses['id_proses']}}">
                    <input type="hidden" name="id_progTa" id="id_progTa" value="{{$progTa->id}}">
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-user"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Awal</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control input-circle-left" readonly placeholder="tgl_awal" name="tgl_awal" value="{{$proses['tgl_awal']}}">
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Akhir</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control input-circle-left" readonly placeholder="tgl_akhir" name="tgl_akhir" value="{{$proses['tgl_akhir']}}">
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Nilai DosPem 1</label>
            <div class="col-md-9">
                <div class="input-group">
                @if(isset($proses['nilaiDosen1']))
                    <input type="text" class="form-control input-circle-left" readonly placeholder="nilaiDosen1" value="{{$proses['nilaiDosen1']}}">
                @else
                    <input type="text" class="form-control input-circle-left" readonly placeholder="nilaiDosen1" value="0">
                @endif
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Nilai DosPem 2</label>
            <div class="col-md-9">
                <div class="input-group">
                @if(isset($proses['nilaiDosen2']))
                    <input type="text" class="form-control input-circle-left" readonly placeholder="nilaiDosen2" value="{{$proses['nilaiDosen2']}}">
                @else
                    <input type="text" class="form-control input-circle-left" readonly placeholder="nilaiDosen2" value="0">
                @endif
                    <span class="input-group-addon input-circle-right">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <!-- <label class="col-md-3 control-label">Komentar</label>
            <div class="col-md-9">
                <textarea name="komen" class="form-control" id="komen" cols="30" rows="10">{{$proses['komen']}}</textarea>   
            </div> -->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-hide hide"></i>
                        <span class="caption-subject font-hide bold uppercase">Keterangan</span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline">
                        </div>
                    </div>
                </div>
                <div class="portlet-body" id="chats">
                    <div class="scroller" style="height: 255px;overflow: scroll;" data-always-visible="1" data-rail-visible1="1">
                        <ul class="chats">
                            @if(!empty($proses['komen']))
                            <li class="in">
                                <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
                                <div class="message">
                                    <span class="arrow"> </span>
                                    <a href="javascript:;" class="name"> Irno </a>
                                    <small style="font-size:7pt" class="datetime"> Mahasiswa </small>
                                    <span class="body"> {{$proses['komen']}} </span>
                                </div>
                            </li>
                            @endif
                            @if(!empty($proses['komend1']))
                            <li class="in">
                                <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg" />
                                <div class="message">
                                    <span class="arrow"> </span>
                                    <a href="javascript:;" class="name"> Tarno </a>
                                    <small style="font-size:7pt" class="datetime"> Dospem Pertama </small>
                                    <span class="body"> {{$proses['komend1']}} </span>
                                </div>
                            </li>
                            @endif
                            @if(!empty($proses['komend2']))
                            <li class="in">
                                <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg" />
                                <div class="message">
                                    <span class="arrow"> </span>
                                    <a href="javascript:;" class="name"> Sukadi </a>
                                    <small style="font-size:7pt" class="datetime"> Dospem Kedua </small>
                                    <span class="body"> {{$proses['komend2']}} </span>
                                </div>
                            </li>
                            @endif

                        </ul>
                    </div>
                    @if($proses['status'] != 1)
                    <div class="chat-form">
                        <div class="input-cont">
                            <input class="form-control" name="komen" id="komen" type="text" placeholder="Masukan Keterangan..." /> </div>
                        <div class="btn-cont">
                            <span class="arrow"> </span>
                            <a onClick="addKomendsn()" class="btn blue icn-only">
                                <i class="fa fa-check icon-white"></i>
                            </a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Upload</label>
            <div class="col-md-9">
                <input type="file" id="upload" name="upload">   
            </div>
        </div>
        <hr>
        @if(!empty($proses['upload']))
        <span class="item-box">
            <span class="item"> 
            <a href="{{url('/uploads/dokumen/Usr_'.Auth::user()->id.'/'.$proses['upload'])}}" download>
                <i class="icon-doc" style="font-size:30px"></i> &nbsp;{{$proses['upload']}}
            </a>
            </span>
        </span>
        @endif
        
    </div>
    <div class="modal-footer">
        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Keluar</button>
        <!-- <button type="submit"  class="btn green">Aktif</button> -->
        <button type="button"  onClick="procTa()"  class="btn green">Simpan</button>
    </div>
</form>