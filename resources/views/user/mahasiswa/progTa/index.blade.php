
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                        </div>
                    </div>
                </div>
                    <div id="notif_alert" class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> <p id="notif"> You have some form errors. Please check below.</p> 
                    </div>
                
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <!-- <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                        <i class="fa fa-plus"></i>
                                    </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="items" class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th> BAB </th>
                                    <th> Tgl Awal </th>
                                    <th> Tgl Akhir </th>
                                    <th> Keterangan </th>
                                    <th> Upload </th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($prosesSubmit as $val)
                                <tr class="odd gradeX">
                                    <td>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="checkboxes" value="1" />
                                            <span></span>
                                        </label>
                                    </td>
                                    <td> {{$val['bab']}} </td>
                                    <td> {{$val['tgl_awal']}} </td>
                                    <td> {{$val['tgl_awal']}}</td>
                                    <td class="center"> {{$dropStatus[$val['status']]}} </td>
                                    <td class="center"> {{$val['upload']}}  </td>
                                    <td>
                                         <button type="button" class="btn red btn-sm mt-ladda-btn ladda-button btn-circle" onClick="detail('{{$val['id_proses']}}',{{$progTa->id}})" data-style="zoom-in">
                                            <span class="ladda-label"><i class="fa fa-edit"></i> Detail</span>
                                          </button>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
   
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal Title</h4>
            </div>
            <div class="modal-body" id="content"> 
                <div id="loading">
                    <img src="/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                </div>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

function detail(e,x){
    var id_proses = e;
    var id_progTa = x;
// alert(id_dosen_mhs);
    $('#basic').modal('show')
    $.ajax({
        type: 'GET',
        url : '{{url('getModalProgTa/')}}',
        data: {id_proses:id_proses,id_progTa:id_progTa},
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            $('#loading').hide()
            $('#content').html(data)
        }
    });    
}

function procTa(){
    var token = $('meta[name="csrf-token"]').attr('content');
    var komen = $('#komen').val();
    var id_progTa = $('#id_progTa').val();
    var id_proses = $('#id_proses').val();
    var fd = new FormData();
    var files = $('#upload')[0].files[0];
    fd.append('upload',files);
    fd.append('_token',token);
    fd.append('komen',komen);
    fd.append('id_progTa',id_progTa);
    fd.append('id_proses',id_proses);

    $.ajax({
        type: 'POST',
        url : $('#formProgTa').attr('action'),
        data: fd,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
	    		
        },
        success: function(data){
            alert(data.notif)
            $('#basic').modal('hide')
            $('#loading').hide()
            proses_ta(id_user)
            $('#notif_alert').show()
            $('#notif').html(data.notif)
            $('#notif_alert').addClass(data.alert)
        }
    });    
}

function addKomendsn(){
    var token = $('meta[name="csrf-token"]').attr('content');
    var id_progTa = $('#id_progTa').val();
    var id_proses = $('#id_proses').val();
    var komen = $('#komen').val();
    
    $.ajax({
        type: 'GET',
        url : '{{url("add-komen")}}',
        data: {id_progTa:id_progTa,id_proses:id_proses,komen:komen},
        dataType: 'json',
        beforeSend:function(){
            $('#loading').show()
        },
        error: function(jqXHR, textStatus, errorThrown){
            $('#loading').hide()
        },
        success: function(data){
            $('#loading').hide()
            detail(id_proses,id_progTa)
        }
    });  

}

</script>
