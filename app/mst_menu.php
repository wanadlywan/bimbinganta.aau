<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_menu extends Model
{
    //
    protected $table = "mst_menu";
    protected $guardable = "id";

    public static function cekRole($id_role){
        $menu = mst_menu::select('*')
                ->join('access_menu','mst_menu.id','=','access_menu.id_menu')
                ->where('id_role',$id_role)
                ->orderBy('sort','ASC')
                ->get();
        return $menu;
    }
}
