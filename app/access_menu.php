<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class access_menu extends Model
{
    //
    protected $table = "access_menu";
    protected $guardable = "id";
}