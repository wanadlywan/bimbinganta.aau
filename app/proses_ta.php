<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proses_ta extends Model
{
    //
    protected $table = "proses_ta";
    protected $fillable = [
        'proses_submit','proses_bimbingan'
    ];
}
