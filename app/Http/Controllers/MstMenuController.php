<?php

namespace App\Http\Controllers;

use App\mst_menu;
use Illuminate\Http\Request;

class MstMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mst_menu  $mst_menu
     * @return \Illuminate\Http\Response
     */
    public function show(mst_menu $mst_menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mst_menu  $mst_menu
     * @return \Illuminate\Http\Response
     */
    public function edit(mst_menu $mst_menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mst_menu  $mst_menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mst_menu $mst_menu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mst_menu  $mst_menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(mst_menu $mst_menu)
    {
        //
    }
}
