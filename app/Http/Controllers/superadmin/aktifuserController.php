<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\tbl_dosen_mhs as DosenMhs;
use App\proses_ta as ProsesTa;
use App\mst_bab as BAB;

class aktifuserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function coba(){
        return view('coba');
    } 
    public function index()
    {
        $data['title']  = 'Aktivasi user';
        $data['user']  = User::all();
        $data['mahasiswa']  = User::where('role',1)->get();
        $data['dosen']  = User::where('role',2)->get();
        $data['drop_role'] = ['1'=>'Mahasiswa','2'=>'Dosen'];
        $data['drop_status'] = ['0'=>'Tidak aktif','1'=>'Aktif'];
        $data['drop_status_alert'] = ['0'=>'label-danger','1'=>'label-success'];

        return view('superadmin.aktifuser.index',$data);
    }

    public function getModalUser(Request $request)
    {
        $data['mahasiswa']  = User::find($request->id);
        $data['dosen']  = User::where('role',2)->get();
        $data['dsnMhs'] = DosenMhs::where('id_mhs',$request->id)->first();

        return view('superadmin.aktifuser.modal',$data);
    }

    public function procUserMhs(Request $request){

        if($request->status == 1){
            $result = ['notif'=>'User sudah aktif','alert'=>'alert-warning'];
            return $result;
        }

        $update = User::find($request->id_mhs)->update([
                    'status'  => 1
                  ]);

        if($update){
            $insertDosenMhs = new DosenMhs;
            $insertDosenMhs->id_mhs   = $request->id_mhs;
            $insertDosenMhs->id_dosen1 = $request->id_dosen1;
            $insertDosenMhs->id_dosen2 = $request->id_dosen2;
            $insertDosenMhs->save();

            $data = $insertDosenMhs;

            $id_proses = 'IP'.rand(1,981).'-'.$data->id;
            $dataBab = BAB::all();
            $prosesSubmit = 
            [[
                'id_proses' => $id_proses.'Bab1',
                'kode' => '',
                'bab' => $dataBab[0]->bab,
                'tgl_awal' => $dataBab[0]->tgl_awal,
                'tgl_akhir' => $dataBab[0]->tgl_akhir,
                'status' => '0',
                'upload' => '',
                'komen' => '',
                'komend1' => '',
                'komend2' => '',
            ],[
                'id_proses' => $id_proses.'Bab2',
                'kode' => '',
                'bab' => $dataBab[1]->bab,
                'tgl_awal' => $dataBab[1]->tgl_awal,
                'tgl_akhir' => $dataBab[1]->tgl_akhir,
                'status' => '0',
                'upload' => '',
                'komen' => '',
                'komend1' => '',
                'komend2' => '',

            ],[
                'id_proses' => $id_proses.'Bab3',
                'kode' => '',
                'bab' => $dataBab[2]->bab,
                'tgl_awal' => $dataBab[2]->tgl_awal,
                'tgl_akhir' => $dataBab[2]->tgl_akhir,
                'status' => '0',
                'upload' => '',
                'komen' => '',
                'komend1' => '',
                'komend2' => '',
            ],[
                'id_proses' => $id_proses.'Bab4',
                'kode' => '',
                'bab' => $dataBab[3]->bab,
                'tgl_awal' => $dataBab[3]->tgl_awal,
                'tgl_akhir' => $dataBab[3]->tgl_akhir,
                'status' => '0',
                'upload' => '',
                'komen' => '',
                'komend1' => '',
                'komend2' => '',
            ],[
                'id_proses' => $id_proses.'Bab5',
                'kode' => '',
                'bab' => $dataBab[4]->bab,
                'tgl_awal' => $dataBab[4]->tgl_awal,
                'tgl_akhir' => $dataBab[4]->tgl_akhir,
                'status' => '0',
                'upload' => '',
                'komen' => '',
                'komend1' => '',
                'komend2' => '',
            ]];

            $insertProsesTa = new ProsesTa;
            $insertProsesTa->id_dosen_mhs = $data->id;
            $insertProsesTa->proses_submit = json_encode($prosesSubmit);
            $insertProsesTa->save();

            $result = ['notif'=>'User berhasil di aktifkan','alert'=>'alert-success'];

        }else{
            $result = ['notif'=>'User gagal di aktifkan','alert'=>'alert-danger'];
        }
        return json_encode($result);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
