<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\user_detail as UserDetail;
use App\tbl_dosen_mhs as DosenMhs;
use App\proses_ta;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id_user = Auth::user()->id;
        $role = Auth::user()->role;
        $user = User::all();
        $data['user'] = User::find($id_user);
        $data['userDetail'] = UserDetail::where('id_user',$id_user)->first();
        $user_img = UserDetail::select('id_user','img')->get();
        $user_name = User::select('id','name')->get();
        foreach($user_name as $val){
            $data['user_name'][0] = '-';
            $data['user_name'][$val->id] = $val->name;
        }
        foreach($user_img as $valImg){
            $data['user_img'][0] = '-';
            $data['user_img'][$valImg->id_user] = $valImg->img;
        }
        
        $data['dropStatus'] = ["1"=>"Approved","0"=>"Not Approved","2"=>"on Proggress","3"=>"Waiting"];
        foreach($user as $value){
            $data['dropUser'][$value->id] = $value->name;
        }
        
        if($role == 3){

            return redirect('/aktif-user');

        }else if($role == 1){
            
            $userMhs = $data['userMhs'] = DosenMhs::where('id_mhs',$id_user)->first();
            $countPTa = 0;
            $countBTa = 0;
            $nilaitot = 0;

            if(!empty($userMhs->id)){
                $data['prosesTa'] = proses_ta::where('id_dosen_mhs',$userMhs->id)->first();
                $arryPTa = json_decode($data['prosesTa']->proses_submit);
                $arryBTa = json_decode($data['prosesTa']->proses_bimbingan);
                // dd($arryPTa);
                foreach($arryPTa as $key => $val){
                    if($val->status == 1){
                        $countPTa = count($val); 
                    }
                    if(!empty($val->nilaiDosen1)){
                        $nilaitot += $val->nilaiDosen1;
                    }
                    if(!empty($val->nilaiDosen2)){
                        $nilaitot += $val->nilaiDosen2;
                    }
                }
                
                // dd($nilaitot);
                // $countBTa = count($arryBTa); 
                
                
                $data['persenPTa'] = ($countPTa/5)*100;
                // $data['totalBTa'] = $countBTa;
                $data['progPTa'] = ($nilaitot/1000)*100;
            }
            // dd(($countBTa/5)*100);
            return view('user.mahasiswa.index',$data);
        }else if($role == 2){
            $data['list_mhs'] = DosenMhs::where('id_dosen1',$id_user)->orWhere('id_dosen2',$id_user)->get();

            $data['jml_mhs'] = count($data['list_mhs']);
            return view('user.dosen.index',$data);
        }
    }

    public function userProfile(Request $request){
        $data['id_user'] = $id = $request->id;
        $data['user'] = UserDetail::where('id_user',$id)->first();
        return view('user.user-detail',$data);

    }
}
