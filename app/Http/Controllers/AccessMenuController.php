<?php

namespace App\Http\Controllers;

use App\access_menu;
use Illuminate\Http\Request;

class AccessMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\access_menu  $access_menu
     * @return \Illuminate\Http\Response
     */
    public function show(access_menu $access_menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\access_menu  $access_menu
     * @return \Illuminate\Http\Response
     */
    public function edit(access_menu $access_menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\access_menu  $access_menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, access_menu $access_menu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\access_menu  $access_menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(access_menu $access_menu)
    {
        //
    }
}
