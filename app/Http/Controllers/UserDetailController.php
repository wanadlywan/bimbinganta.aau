<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\user_detail;


class UserDetailController extends Controller
{
    public function store(Request $request){
        if($request->flag == 1){
            $update = user_detail::where('id_user',$request->id_user)->update([
                'nik'=>$request->nik,
                'prodi'=>$request->prodi,
                'judulTa'=>$request->judulTa,
            ]);
        }else{
            $update = user_detail::where('id_user',$request->id_user)->update([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'address'=>$request->address,
                'no_phone'=>$request->no_phone,
                'birth_date'=>$request->birth_date,
            ]);
        }
        if($update){
            $result = ['notif'=>'data berhasil di simpan','alert'=>'alert-success'];

        }else{
            $result = ['notif'=>'data gagal di simpan','alert'=>'alert-danger'];
        }
        return $result;
    }

    public function storeUpload(Request $request){
        $id_cek = Auth::user()->id;
        if($file = $request->hasFile('upload')){    
            $file = $request->file('upload');
            $folderName = 'Usr_'.$id_cek;

            if(!is_dir('./uploads/dokumen/'.$folderName)){
                mkdir('./uploads/dokumen/'.$folderName,0777);
            }
            
            $filename = str_random(5).'_'.$id_cek.'_'.$file->getClientOriginalName();
            $destinationPath = public_path() . "/uploads/dokumen/".$folderName;
            $file->move($destinationPath, $filename);

            $update = user_detail::where('id_user',$id_cek)->update([
                'img' => $filename
            ]);
            if($update){
                $result = ['notif'=>'data berhasil di simpan','alert'=>'alert-success'];
    
            }else{
                $result = ['notif'=>'data gagal di simpan','alert'=>'alert-danger'];
            }
            return $result;
        }
    }

    // public function storePassword(Request $request){
    //     $user = User::find(Auth::user()->id);
    //     $old_password = Hash::make($request->old_password);
    //     if($old_password == $user->password){
    //         echo 'aa';
    //     }else{

    //         echo 'asaa';
    //     }
    // }
}
