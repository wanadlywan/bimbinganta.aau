<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_bab extends Model
{
    //
    protected $table = 'mst_bab';
    protected $guardable = "id";
    protected $fillable = ["tgl_awal","tgl_akhir"];
}
